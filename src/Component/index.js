import { lazy } from "react";

export const Store = lazy(() => import("./Store/Store"));
export const LazyStoreView = lazy(() => import("./Store/StoreView"));
export const LazyStoreViewOnScroll = lazy(() =>
  import("./Store/StoreViewOnScroll")
);
