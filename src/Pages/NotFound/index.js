import React, { useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";

const NotFound = () => {
  const navigate = useNavigate();
  const location = useLocation();
  console.log("location obj", location);
  const { pathname } = location;

  // useEffect(() => {
  //   let path = "";
  //   if (pathname.search("/") >= 0) {
  //     path = "/";
  //   }
  //   navigate(path);
  // }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="col-12 p-5">
          <h5>404 The Requested Page Not Found</h5>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
