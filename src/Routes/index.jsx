// import Store from "./Component/Store/Store";

import { Routes, Route } from "react-router-dom";
import { LazyStoreView, LazyStoreViewOnScroll, Store } from "../Component";

import { NotFound } from "../Pages";

export default function RouteComponent() {
  const routesArr = [
    {
      path: "/",
      element: <Store />,
    },
    {
      path: "/store-view",
      element: <LazyStoreView />,
    },
    {
      path: "/get-inventory-list",
      element: <LazyStoreViewOnScroll />,
    },

    {
      path: "*",
      element: <NotFound />,
    },
  ];

  return (
    <Routes>
      {routesArr?.map((item, index) => (
        <Route path={item?.path} element={item?.element} key={index} />
      ))}
    </Routes>
  );
}

//http://192.168.10.13:38955/get-inventory-list?token=8185|Z6Mwm7m2VpRfxJZ74Gs580HFbofGb3Bc65N9SlvX&id=1169
